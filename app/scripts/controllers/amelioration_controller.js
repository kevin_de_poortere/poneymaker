Poney.AmeliorationController = Ember.ObjectController.extend({
    needs: ['application'],
    actions   : {
        buyAmelioration : function(item) {
            //get current poney, upgrade him
            var poney = this.get('controllers.application.model');
            if ( (poney.nb - item.prix ) >= 0 ) {
                var newLevel = poney.level + 1;
                poney.set("nom", item.nom);
                poney.set("nb", poney.nb - item.prix);
                poney.set("click", item.click);
                poney.set("level", poney.level + 1);
                poney.set("image", "images/poney" + newLevel + ".png");

                window.localStorage.setItem('poney', JSON.stringify(poney) );

                var amelioration = this.get('model');
                console.log(amelioration);
                var indexToDelete = amelioration.indexOf(item);
                console.log(indexToDelete);
                amelioration.splice(indexToDelete, 1);

            }else
                alert('C\'est trop cher');
        }
    }
});
