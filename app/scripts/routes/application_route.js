Poney.ApplicationRoute = Ember.Route.extend({

    model: function () {

    	if(currentPoney = window.localStorage.getItem('poney')) {
    		return Poney.Poney.create(JSON.parse(currentPoney));
    	}
    	else {
	        return Poney.Poney.create({
	            nom : "Petit Poney",
	            nb : 1,
	            level : 1,
	            click : 1,
	            image : "images/poney1.png"
	        });
	    }
    }
});