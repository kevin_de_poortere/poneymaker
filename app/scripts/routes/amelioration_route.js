Poney.AmeliorationRoute = Ember.Route.extend({
    model: function () {

        return [
            Poney.Amelioration.create({
                id    : 1,
                nom   : "Golden Poney",
                prix  : 100,
                level : 1,
                click : 2
            }),

            Poney.Amelioration.create({
                id    : 2,
                nom   : "Super Golden Poney",
                prix  : 500,
                level : 2,
                click : 4
            }),

            Poney.Amelioration.create({
                id    : 3,
                nom   : "Super Poney",
                prix  : 1000,
                level : 3,
                click : 6
            }),

            Poney.Amelioration.create({
                id    : 4,
                nom   : "Super Golden Poney",
                prix  : 1500,
                level : 4,
                click : 8
            }),

            Poney.Amelioration.create({
                id    : 5,
                nom   : "Super Golden Poney Tribal",
                prix  : 4000,
                level : 5,
                click : 16
            }),

            Poney.Amelioration.create({
                id    : 6,
                nom   : "Ninja Poney",
                prix  : 8500,
                level : 6,
                click : 32
            }),

            Poney.Amelioration.create({
                id    : 7,
                nom   : "Girly Poney",
                prix  : 10000,
                level : 7,
                click : 64
            }),

            Poney.Amelioration.create({
                id    : 8,
                nom   : "Unicorn Poney",
                prix  : 20000,
                level : 8,
                click : 128
            }),

            Poney.Amelioration.create({
                id    : 9,
                nom   : "Super Unicorn Poney",
                prix  : 30000,
                level : 9,
                click : 256
            }),

            Poney.Amelioration.create({
                id    : 10,
                nom   : "Pegase Poney",
                prix  : 50000,
                level : 10,
                click : 512
            }),

            Poney.Amelioration.create({
                id    : 11,
                nom   : "Super Pegase Poney",
                prix  : 99000,
                level : 11,
                click : 1024
            }),

            Poney.Amelioration.create({
                id    : 12,
                nom   : "Ultimate Poney",
                prix  : 1000000,
                level : 12,
                click : 4096
            }),
        ];
    }
});